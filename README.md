# README #

Phonebilling test to allow you calculate rates in cents based on the time consumed on the phone.

### Setup ###

* git clone repository
* run composer install
* open index file in browser to view result in json

### How to add logs string ###

* Open  project config folder;
* Open Config class;
* Set the options property - please see example in project skeleton;

### Who do I talk to? ###

* Repo owner or admin - (ben.igbokwe@hotmail.com)