<?php

namespace Config;

class Config
{
    /**
     * @var array
     */
    private static $options = [
        'string' => "00:01:07,400-234-090 00:05:01,701-080-080 00:05:00,400-234-090 00:06:07,400-234-080 00:06:07,400-234-070",
    ];

    /**
     * @param $key
     * @return mixed
     */
    public static function get($key)
    {
        return self::$options[$key];
    }
}
