<?php

require_once('bootstrap.php');
$string = \Config\Config::get('string');

$service = Application\Factory\PhoneBillingFactory::getService();

try{
    $message = $service->solution($string);
} catch (Exception $ex) {
    $message = $ex->getMessage();
}

echo $message;