<?php

namespace Application\Service;

use Application\Log;
use Application\PhoneBilling;

class PhoneBillingService
{
    /**
     * @var Log
     */
    private $log;

    /**
     * @var PhoneBilling
     */
    private $billing;

    public function __construct(Log $log, PhoneBilling $billing)
    {
        $this->log     = $log;
        $this->billing = $billing;
    }

    /**
     * @param $string
     * @return string
     * @throws \Exception
     */
    public function solution($string)
    {
        $lines     = $this->log->getLines($string);
        $calculate = $this->billing->calculate($lines);

        if (is_array($calculate)) {
            return json_encode($calculate);
        }

        throw new \Exception('Error: Phone billing calculations could not be resolved');
    }
}