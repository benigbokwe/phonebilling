<?php

namespace Application;

/**
 * Class PhoneNumber
 * @package Application
 */
class PhoneNumber
{
    /**
     * @param $allLongCalls
     * @return mixed
     */
    public function isQualified($allLongCalls)
    {
        $allTopCalls = $this->getAllTopCalls($allLongCalls);

        // sort array by key
        // this places the smallest number at the top
        sort($allTopCalls);

        reset($allTopCalls);

        //return top array index with lowest numerical value
        return current($allTopCalls);
    }

    /**
     * @param $allLongCalls
     * @return array
     */
    private function getAllTopCalls($allLongCalls)
    {
        $qualified = array();
        $maxValue   = 0;

        foreach ($allLongCalls as $key => $value) {
            if ($value >= $maxValue) {
                $maxValue = $value;
                $qualified[] = $key;
            }
        }

        return $qualified;
    }
}
