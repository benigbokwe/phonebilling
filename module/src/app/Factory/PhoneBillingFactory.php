<?php

namespace Application\Factory;

use Application\Log;
use Application\PhoneBilling;
use Application\PhoneNumber;
use Application\Service\PhoneBillingService;

/**
 * Class PhoneBillingFactory
 * @package Application\Factory
 */
class PhoneBillingFactory
{
    /**
     * @return PhoneBillingService
     */
    public static function getService()
    {
        return new PhoneBillingService(new Log(), new PhoneBilling(new PhoneNumber()));
    }
}
