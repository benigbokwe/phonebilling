<?php

namespace Application;


interface LogInterface
{
    /**
     * @param $string
     * @return mixed
     */
    public function getLines($string);
}
