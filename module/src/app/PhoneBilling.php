<?php

namespace Application;

/**
 * Class PhoneBilling
 * @package Application
 */
class PhoneBilling
{
    /**
     * @var PhoneNumber
     */
    private $phoneNumber;

    /**
     * Minimum seconds to be billed on per minutes basis (5 minutes)
     */
    const MIN_SECONDS = 300;

    /**
     * Free call charge
     */
    const FREE_CALL = 0;

    /**
     * @param PhoneNumber $phoneNumber
     */
    public function __construct(PhoneNumber $phoneNumber)
    {
        //Sets the default timezone used by all date/time functions in a script
        date_default_timezone_set('Europe/London');

        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @param $lines
     * @return array
     * @throws \Exception
     */
    private function getTimePerPhone($lines)
    {
        $timePerPhone = array();
        //date_default_timezone_set('Europe/London');

        if (is_array($lines)) {
            foreach ($lines as $line) {
                // validate line
                if (Validator::isValidLine($line)) {
                    @list($time, $phone) = explode(",", $line);

                    // validate phone and time formats
                    if (Validator::isValidPhone($phone) && Validator::isValidTime($time)) {
                        // convert textual datetime into unix timestamp
                        // add timestamp into array of number
                        $timePerPhone[$phone][] = strtotime($time);
                    }
                }
            }

            return $timePerPhone;
        }

        throw new \Exception('Error: please check the supplied lines string');
    }

    /**
     * @param $lines
     * @return array
     * @throws \Exception
     */
    public function calculate($lines)
    {
        $timePerNumber    = $this->getTimePerPhone($lines);
        $calculateNumRate = array();
        $allLongCalls     = array();

        foreach ($timePerNumber as $phone => $time) {
            // Calculate the total time consumed by each number

            $timeSpent = array_sum($time);
            // get the hours, minutes and seconds
            $hours   = ltrim(date('H', $timeSpent), 0);
            $minutes = ltrim(date('i', $timeSpent), 0);
            $seconds = ltrim(date('s', $timeSpent), 0);

            $totalSeconds = ($hours * 3600)  + ($minutes * 60)  + $seconds;

            if ($totalSeconds < self::MIN_SECONDS) {
                $rate = $totalSeconds * 3;
            } else {
                // minutes is more than 5 minutes then apply per minute charge
                $rate = $minutes * 150;
            }

            $allLongCalls[$phone]   = $minutes;
            $calculateNumRate[$phone] = $rate;
        }

        // check if number is qualified for promo
        $qualifiedNumber = $this->phoneNumber->isQualified($allLongCalls);

        // if number exists in calculated number rate array, then apply 0 call charge
        if (array_key_exists($qualifiedNumber, $calculateNumRate)) {
            $calculateNumRate[$qualifiedNumber] = self::FREE_CALL;
        }

        return $calculateNumRate;
    }
}
