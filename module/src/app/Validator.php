<?php

namespace Application;

class Validator
{
    /**
     * @param $phone
     * @return bool
     * @throws \Exception
     */
    public static function isValidPhone($phone)
    {
        // every phone number follows the format "nnn-nnn-nnn" strictly;
        // there are no leading zeros;
        if (preg_match("/^([1-9]{1}+[0-9]{2})-[0-9]{3}-[0-9]{3}$/", $phone)) {
            // phone is valid
            return true;
        }

        throw new \Exception('Phone number format is not valid. Expected format: "nnn-nnn-nnn" with no leading zeros');
    }

    /**
     * @param $time
     * @return bool
     * @throws \Exception
     */
    public static function isValidTime($time)
    {
        //the duration of every call follows the format "hh:mm:ss" strictly
        // (00 ≤ hh ≤ 99, 00 ≤ mm, ss ≤ 59);
        if (preg_match("/^[0-9]{2}:[0-5][0-9]:[0-5][0-9]$/", $time)) {
            // time is valid
            return true;
        }

        throw new \Exception('Time format is not valid. Expected format: "hh:mm:ss"');
    }

    /**
     * @param $line
     * @return bool
     * @throws \Exception
     */
    public static function isValidLine($line)
    {
        //each line follows the format "hh:mm:ss,nnn-nnn-nnn" strictly;
        // there are no empty lines and spaces.
        if (preg_match("/^([0-9]{2}:[0-9]{2}:[0-9]{2}),([0-9]{3}-[0-9]{3}-[0-9]{3})$/", $line)) {
            // line is valid
            return true;
        }

        throw new \Exception('Line is not valid. Expected format: "hh:mm:ss,nnn-nnn-nnn"');
    }
}
