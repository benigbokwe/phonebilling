<?php

namespace Application;

use Config\Config;

/**
 * Class CallLog
 * @package Application
 */
class Log implements LogInterface
{
    /**
     * @param $string
     * @return array
     */
    public function getLines($string)
    {
        return explode(" ", $string);
    }
}
